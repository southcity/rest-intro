import express from 'express'
import listRouter from './routes/lists'
import tasks from './data'

const PORT = 3000

const app = express()

app.use(express.json())

app.use('/api/v1/lists', listRouter)

app.listen(PORT, () => {
  console.info(`Server running on port ${PORT}`)
})
