import { Router } from 'express'
import { Lists, Tasks } from '../data'

const router = new Router()

router.post('/', (req, res) => {
  const { id, title } = req.body

  if (typeof id !== 'number' || typeof title !== 'string') {
    return res
      .status(400)
      .send('A list must contain following properties: id, title')
  }

  if (Lists.some((list) => list.id == id)) {
    return res.status(400).send(`A list with id '${id}' already exists`)
  }

  Lists.push({ id, title })

  res.status(201).location(`${req.baseUrl}/${id}`).send()
})

router.get('/:listId', (req, res) => {
  const { listId } = req.params

  const list = Lists.find((list) => list.id == listId)

  if (list) {
    res.json(list)
  } else {
    res.status(404).send(`No list with id '${listId}' was found.`)
  }
})

router.delete('/:listId', (req, res) => {
  const { listId } = req.params
  const index = Lists.findIndex((list) => list.id == listId)

  if (index != -1) {
    let i = Tasks.length

    while (i--) {
      if (Tasks[i].listId === Lists[index].id) {
        Tasks.splice(i, 1)
      }
    }

    Lists.splice(index, 1)
    console.log(Tasks)
    res.json(Lists)
  } else {
    res.status(404).send(`List with id '${id}' not found.`)
  }
})

router.get('/:listId/tasks', (req, res) => {
  const { listId } = req.params
  const tasks = Tasks.filter((task) => task.listId == listId)

  res.json(tasks)
})

router.post('/:listId/tasks', (req, res) => {
  const { listId } = req.params

  if (!Lists.some((list) => list.id == listId)) {
    return res.status(400).send(`List with id '${listId}' not found.`)
  }

  const { id, title, isDone } = req.body

  if (
    typeof id !== 'number' ||
    typeof title !== 'string' ||
    typeof isDone !== 'boolean'
  ) {
    return res
      .status(400)
      .send(`A task must must contain following properties: id, title, isDone`)
  }

  if (Tasks.some((task) => task.id === id)) {
    return res.status(400).send(`A task with id '${id}' already exists`)
  }

  Tasks.push({ id, title, isDone, listId })

  res.status(201).location(`${req.baseUrl}/${listId}/tasks/${id}`).send()
})

router.get('/:listId/tasks/:taskId', (req, res) => {
  const { taskId } = req.params
  const task = Tasks.find((task) => task.id == taskId)

  if (task) {
    res.json(task)
  } else {
    res.status(400).send(`Task with id '${taskId}' not found.`)
  }
})

router.delete('/:listId/tasks/:taskId', (req, res) => {
  const { taskId } = req.params
  const index = Tasks.findIndex((task) => task.id == taskId)

  if (index != -1) {
    Tasks.splice(index, 1)
    res.json(Tasks)
  } else {
    res.status(404).send(`Task with id '${taskId}' not found.`)
  }
})

export default router
