export const Lists = [
  {
    id: 1,
    title: 'DCST2002',
  },
]

export const Tasks = [
  {
    id: 1,
    title: 'Les leksjon',
    isDone: false,
    listId: 1,
  },
  {
    id: 2,
    title: 'Møt opp på forelesning',
    isDone: false,
    listId: 1,
  },
  {
    id: 3,
    title: 'Gjør øving',
    isDone: false,
    listId: 1,
  },
]
